# Screw Default Buttons version 1.2.3

ScrewDefaultButtons is a simple jQuery plugin allowing you to replace the browser defaults for radio buttons and checkboxes with your own custom design (image).

Made by [Matt Solano](http://mattsolano.com)

[Version 2  is here](https://github.com/mattSOLANO/ScrewDefaultButtonsV2) and it works completely differently.

## Setup

1 - Link. 

After downloading the plugin, link to both the jQuery library as well as the ScrewDefaultButtons plugin. You may do this by placing the following lines in the head of your HTML document:

```
<script src="http://code.jquery.com/jquery-latest.min.js" ></script>
<script src="js/jquery.screwdefaultbuttons.js" ></script> 
```

2 - Run. 

Apply the .screwDefaultButtons() method to your radio buttons and/or checkboxes (Note: Must use separate calls). Here is where you will define your replacement images with their height and width. Below is an example:

```
<script>;
    $(document).ready(function(){
          $('input:radio').screwDefaultButtons({
             checked: "url(images/radio_Checked.jpg)",
             unchecked: "url(images/radio_Unchecked.jpg)",
             width: 85,
             height: 85
          });

          $('input:checkbox').screwDefaultButtons({
             checked: "url(images/checkbox_Checked.jpg)",
             unchecked: "url(images/checkbox_Unchecked.jpg)",
             width: 85,
             height: 85
          });
    });
</script> 
```

3 - Style. 

To add things like margin and positioning, you may use the classes .styledRadio and .styledCheckbox in your own css. The following is an example of how it may look:

```

.styledRadio {
    /* Your styles here */
}

.styledCheckbox {
    /* Your styles here */
}
```

## More Functionality

Select All - 

In the case of checkboxes, you have the ability to indicate a "select all" button. Checking this box will select all other checkboxes; Unchecking it will clear all fields; It will also clear if the user unchecks another box. All you have to do is indicate which button should be given this ability:

```
$('input:checkbox').screwDefaultButtons({
       checked: "url(images/checkbox_Checked.jpg)",
       unchecked: "url(images/checkbox_Unchecked.jpg)",
       width: 85,
       height: 85,
       selectAll: "#selectAllBtn"
});
```

Preselected Buttons - 

The 'checked' attribute indicates that a input should be preselected when the page loads. ScrewDefaultButtons will recognize this and make sure your custom radio buttons and checkboxes are also preselected.

```
<input type="radio" checked="checked">

<input type="checkbox" checked="checked"> 
```

Disabled Buttons - 

Custom radio buttons and checkboxes may be disabled by default. In order to do this, you must also specify the disabled version and/or disabled-checked version(when the button is selected and disabled by default). Then, you may apply the disabled attribute as usual in your HTML.

```
$('input:checkbox').screwDefaultButtons({
       checked: "url(images/checkbox_Checked.jpg)",
       unchecked: "url(images/checkbox_Unchecked.jpg)",
       disabled: "url(images/checkbox_Disabled.jpg)",
       disabledChecked: "url(images/checkbox_disabledChecked.jpg)",
       width: 85,
       height: 85,

});


<input type="checkbox" disabled="disabled" > 
```

Labels with 'for' - 

Form labels may be given the 'for' attribute, associating it with an input having a matching id. Feel free to continue doing this as ScrewDefaultButtons will work in the same way. 

```
<input type="radio" id="large"> <label for="large">Large</label> 
```

Inherits 'class' and 'onclick' - 

Due to popular demand, ScrewDefaultButtons now inherits class and onclick attributes applied to the radio buttons and checkboxes in your HTML. This can be used for both styling as well as ensuring that any events you may have created are still triggered.

```
<input type="radio" class="myRadioButton" >
<input type="checkbox" onclick="myEvent();" > 
```


